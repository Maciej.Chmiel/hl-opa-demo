ENVIRONMENTS = {
  "dev" = {
    bucket_name        = "hl-opa-demo"
    src_directory_path = "./dist"
    resource_tags = {
      application-id   = "hl-opa-demo"
      environment-type = "dev"
      app-name         = "hl-opa-demo"
    }
  }
}
