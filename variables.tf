variable "DEPLOY_ENV" {
  type = string
}

variable "ENVIRONMENTS" {
  type = map(object({
    bucket_name        = string
    origin_id          = string
    src_directory_path = string
    resource_tags      = map(any)
  }))
}
