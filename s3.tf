variable "bucket_name" {
  type        = string
  description = "S3 bucket name"
}

variable "bucket_acl" {
  type        = string
  default     = "private"
  description = "S3 bucket ACL"
}

variable "bucket_versioning" {
  type        = string
  default     = "Disabled"
  description = "S3 bucket versioning"
}

variable "src_directory_path" {
  type        = string
  description = "Directory path to source files"
}

variable "default_root_object" {
  type        = string
  default     = "index.html"
  description = "Cloudfront distribution default root object"
}



resource "aws_s3_bucket" "s3_bucket" {
  bucket_prefix = var.bucket_name
  tags          = var.resource_tags
}

resource "aws_s3_bucket_acl" "s3_bucket_acl" {
  bucket = aws_s3_bucket.s3_bucket.id
  acl    = var.bucket_acl
}

resource "aws_s3_bucket_versioning" "s3_bucket_versioning" {
  bucket = aws_s3_bucket.s3_bucket.id

  versioning_configuration {
    status = var.bucket_versioning
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "server_side_encryption_configuration" {
  bucket = aws_s3_bucket.s3_bucket.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_object" "html" {
  for_each     = fileset(var.src_directory_path, "**/*.html")
  bucket       = aws_s3_bucket.s3_bucket.id
  key          = each.value
  source       = "${var.src_directory_path}/${each.value}"
  content_type = "text/html"
}


